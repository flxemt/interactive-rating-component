const form = document.getElementById('card-form')
const card = document.getElementById('card')
const submitBtn = document.getElementById('btn-submit')
const inputs = document.querySelectorAll('.inputs input')

function handleRadioButtonChange() {
  submitBtn.disabled = false
}

function handleSubmit(event) {
  event.preventDefault()
  const formData = new FormData(form)
  const score = formData.get('score')

  card.innerHTML = `
    <div class="card__summary">
      <img src="/illustration-thank-you.svg" class="summary-img" alt="">
      <div class="card__banner">You selected ${score} out of 5</div>
      <h2 class="card__title card__title--summary">Thank you!</h2>
      <p class="card__text">We appreciate you taking the time to give a rating. If you ever need more support, don’t hesitate to get in touch!</p>
    </div>
  `
}

form.addEventListener('submit', handleSubmit)
inputs.forEach(input => input.addEventListener('change', handleRadioButtonChange))
